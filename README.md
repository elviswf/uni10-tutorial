# Welcome to Uni10 Tutorials! #

This the repository hosting the materials for the Uni01 Tutorials.

Here you can find 

* **pyUni10**: Python wrappers for Uni10.
* **Lectures**: iPython notebooks for lectures.
* Reference Manual for pyUni10.

This tutorial is released under a CC BY-NC-SA 3.0 license. The Uni10
code is released under a LGPL license.

### Setup 
* You need to have Python 2 installed on your computer. If you are using Linux or MacOS X, Python 2 comes with the system.   
  For the lectures, you also need install iPython.  
  As an integrated distribution, we recommend the [Anaconda Scientific Python Distribution](https://store.continuum.io/cshop/anaconda/). 

* Please download  pyUni10 for your architecture [here](https://bitbucket.org/yingjerkao/uni10-tutorial/downloads)  
* Open a shell terminal, move to the pyUni10 directory, and issue the command:  

        python setup.py install  
  On Windows, open a command prompt window and issue the command:  

        python.exe setup.py install  
  Depending on the location of your Python installation, you might need the administrator privilege to install.  

### Dependencies
  pyUni10 requires **BLAS** and **LAPACK** to run.  
  For Linux users, you need to install BLAS and LAPACK for your distribution.  
  For Mac OS X users, BLAS and LAPACK come with the OS, so you don't have to do anything.  
  For Windows users, required libraries are supplied in the pyUni10 package.