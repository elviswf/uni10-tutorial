pyUni10.PRT_ODD
===============

.. py:data:: pyUni10.PRT_ODD
   :annotation: = 1

   Value defines particle parity **odd** in a bosnic system

