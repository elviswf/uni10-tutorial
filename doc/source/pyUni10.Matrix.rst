pyUni10.Matrix
==============

.. py:currentmodule:: pyUni10

.. py:class:: Matrix

   Proxy of C++ uni10::Matrix class

   Class for matrices

.. X py:method:: Matrix( Rnum,  Cnum[, elem], diag=False, on_gpu=False)

.. py:method:: Matrix( Rnum,  Cnum[, elem], diag=False)

.. py:method:: Matrix( Ma) 

   Creates a Matrix object
 
   :param int Rnum: number of Rows
   :param int Cnum: number of Columns
   :param bool diag: Set True for a diagonal matrix
   :param iterable elem: Matrix elements
   :param Matrix Ma: another Matrix object
   
   :return: a Matrix object
   :rtype: Matrix

   
   .. X:param bool on_gpu: Set True for a Matrix stored on GPU

   .. rubric:: Methods
  
 
   .. py:method:: Matrix.col()
        
      Returns the number of columns in *Matrix*

      :return: number of columns in *Matrix*
      :rtype: int 
          
   .. py:method:: Matrix.eigh()

      Diagonalizes a symmetric *Matrix* and returns a tuple of matrices (:math:`D, U`), 
      where :math:`D` is a diagnoal matrix of eigenvalues and :math:`U` is a matrix of  row-vectors of 
      eigenvectors.
 
      :return: * :math:`D` : diagonal matrix of eigenvalues 

               * :math:`U` : matrix of eigenvectors

      :rtype: tuple of Matrix
   
   .. note::  This function will not check wether the matrix is symmetric.
              
              This is a wrapper of the LAPACK function dsyev()

   .. py:method:: Matrix.elemNum()

      Returns the number of elements in *Matrix*
      
      :return: number of elements in *Matrix*
      :rtype: int

   .. py:method:: Matrix.getElem()
      
      Returns the reference to the elements of *Matrix*

      :return: reference to the element of *Matrix*
      :rtype: float * 
   
   .. py:method:: Matrix.getHostElem()

   .. py:method:: Matrix.identity()
      
      Returns an identity matrix

      :return: an identity matrix
      :rtype: Matrix
      
   .. py:method:: Matrix.isDiag()

      Checks whether *Matrix* is diagonal

      :return: True or False
      :rtype: bool

   .. *   .. py:method:: Matrix.isOngpu()
   .. *
   .. *      Checks whether *Matrix* is stored on GPU
   .. *
   .. *      :return: True or False
   .. *      :rtype: bool

   .. py:method:: Matrix.lanczosEigh( psi, max_iter=200, err_tol = 5E-15)

      Find the lowest eigenvalue and the corresponding eigenvector of *Matrix* by Lanczos method.
      
      :param Matrix psi: initial vector (diagonal Matrix) for Lanczos iteration process, and output the final eigenvector
      :param int max_iter: maximum number of iterations
      :param float err_tol: tolerance of error for the iteration process
      :return: lowest eigenvalue of *Matrix*
      :rtype: float

      .. note:: *Matrix* must be symmetric 

   .. py:method:: Matrix.load(filename)
    
      Loads *Matrix* from a binary file named *filename*

      :param str filename: input filename

   .. py:method:: Matrix.norm()
   
      Returns :math:`L^2` norm of *Matrix*
   
      :return: :math:`L^2` norm of *Matrix*
      :rtype: float

   .. py:method:: Matrix.orthoRand()
    
      Generates an orthogonal basis with random elements and assigns it to the elements of *Matrix*
  
      :return: Matrix of orthogonal basis
      :rtype: Matrix 

   .. py:method:: Matrix.randomize()

      Assigns random elements to *Matrix*
  
      :return: Matrix of random elements
      :rtype: Matrix 

   .. py:method:: Matrix.resize(Rnum, Cnum)
     
      Set the dimensions of *Matrix* to (Rnum, Cnum)

      :return: Matrix of size (Rnum, Cnum)
      :rtype: Matrix

   .. py:method:: Matrix.row()

      Returns the number of rows in *Matrix*

      :return: number of rows in *Matrix*
      :rtype: int 
       

   .. py:method:: Matrix.save(filename)

      Saves *Matrix* to a binary file named *filename*

      :param str filename: output filename

   .. *   .. py:method:: Matrix.setElem( elem, on_gpu=False)

   .. py:method:: Matrix.setElem( elem)
      
      Set elements of *Matrix* to elem

      :param elem: data
      :type elem: array of floats

   .. py:method:: Matrix.set_zero()
      
      Set elements of *Matrix* to zero

      :param elem: data
      :type elem: array of floats

   .. py:method:: Matrix.sum()

      Performs the summation of all elements in *Matrix*

      :return: sum of all elements in *Matrix*
      :rtype: float

   .. py:method:: Matrix.svd()
   
      Performs SVD of *Matrix* 

      Factorizes the :math:`m \times n` matrix :math:`A` into two unitary matrices :math:`U` and :math:`V^\dagger`, and a diagonal matrix :math:`\Sigma`
      of singular values (real, non-negative) such that  

      .. math:: 
 
         A= U \Sigma V^{\dagger}


      :return:  * :math:`U` : a :math:`m \times n` row-major matrix 

                * :math:`\Sigma` :  a :math:`n \times n` diagonal matrix

                * :math:`V^{\dagger}`:  a :math:`n \times m` row-major matrix

      :rtype: tuple of Matix

      .. note:: This is a wrapper of the Lapack function dgesvd()
   
   .. py:method:: Matrix.trace()

      Takes the trace of a square matrix *Matrix*
      
      :returns: trace of a square matrix *Matrix*
      :rtype: float
      :raise: RunTimeError if *Matrix* is not a  square matrix

   .. py:method:: Matrix.transpose()

      Performs in-place transpose of *Matrix*. The number of rows and the number of columns are exchanged.
 
      :return: *Matrix*
      :rtype: Matrix
     
