pyUni10.Network
===============

.. py:currentmodule:: pyUni10

.. py:class:: Network

.. py:method:: Network( filename [, tensors] )
   
   Loads the network structure from file. 
  
   :param str filename: file where the network structure is stored
   :param tensors: list of tensors to be put into *Network*
   :type tensors: array of UniTensors
   :return: a Network object
   :rtype: Network

 
   .. rubric:: Methods

   .. py:method:: Network.launch([ name])

      Performs contraction of the tensors in the network, and returns a UniTensor named name (optional).

      :param str name: name of the output UniTensor
      :return: contracted tensor 
      :rtype: UniTensor


   .. py:method:: Network.profile()

      Prints the memory usage of *Network*

      :return: current memory usage of *Network*
      :rtype: str

   .. py:method:: Network.putTensor(idx, T[, force=True]) 

   .. py:method:: Network.putTensor(tname, T[, force=True]) 
  
      Replaces the tensor of index idx / name tname in the network file with T. 
      If the force flag is set, the tensor will be put in the network without 
      reconstructing the pair-wise contraction sequence. 

      :param int idx: sequential index of a tensor in *Network*
      :param str name: name of a tensor in *Network*
      :param UniTensor T: tensor to be put into *Network*
      :param bool force: if set True, the contraction sequence is not reconstructed

   .. py:method:: Network.putTensorT(idx,  T[, force=True]) 

   .. py:method:: Network.putTensorT(tname,  T[, force=True]) 
  
      Replaces the tensor of index idx / name tname in the network file with 
      the *transpose* of  T.  If the force flag is set, the tensor will be put 
      in the network without reconstructing the pair-wise contraction sequence. 

      :param int idx: sequential index of a tensor in *Network*
      :param str name: name of a tensor in *Network*
      :param UniTensor T: tensor to be put into *Network*
      :param bool force: if set True, the contraction sequence is not reconstructed
   

   
   
   
