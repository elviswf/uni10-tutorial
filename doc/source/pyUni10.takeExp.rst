pyUni10.takeExp
===============

.. currentmodule:: pyUni10

.. py:function:: takeExp(a, mat) 

   Returns :math:`\exp(a M)`
   
   :param float a: mulitiplication constant 
   :param Matrix mat: input *Matrix*
   :return: a matrix of :math:`\exp(a M)`
   :rtype: Matrix
