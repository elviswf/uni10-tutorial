pyUni10.PRTF_ODD
================

.. py:data:: pyUni10.PRTF_ODD
   :annotation: = 1

   Value defines particle parity **odd** in a fermionic system


