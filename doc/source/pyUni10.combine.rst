pyUni10.combine
===============

.. py:currentmodule:: pyUni10

.. py:function:: combine(bdtype, bds)

   Combines bonds

   :param BondType tp: BD_IN/BD_OUT
   :param bds: list of bonds
   :type bds: array of bonds
   :return: combined bond
   :rtype: Bond 
