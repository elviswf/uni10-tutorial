pyUni10.PRT_EVEN
================

.. py:data:: pyUni10.PRT_EVEN = 0

   Value defines particle parity **even** in a bosonic system


