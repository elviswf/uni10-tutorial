pyUni10.Qnum
============

.. currentmodule:: pyUni10

.. py:class:: Qnum

   Proxy of C++ uni10::Qnum class

   Class for quntum numbers 
   
.. py:method:: Qnum([ U1=0, prt=PRT_EVEN]) 

.. py:method:: Qnum( q) 

   Creates a Qnum object
 
   :param int U1: U1 quantum number
   :param parityType prt: parity quantum number
   :param Qnum q: Another Qnum object
   :return: a Qnum** object
   :rtype: Qnum**

.. py:method:: QnumF(prtF, [U1=0, prt=PRT_EVEN ]) 

   Construct a Qnum object with fermionic parity

   :param parityFType prtF: fermionic parity quantum number
   :param int U1: U1 quantum number
   :param parityType prt: parity quantum number
   :return: Qnum object
   :rtype: Qnum

   .. rubric:: Methods

   .. py:method:: Qnum.U1()
   
      Returns the U1 quantum number

      :return: U1 quantum number
      :rtype: int 

   .. py:method:: Qnum.assign( [U1=0,  prt=PRT_EVEN ])

      Assign a quantum number to a Qnum object

      :param int U1: U1 quantum number
      :param parityType prt: parity quantum number
      :return: a Qnum object
      :rtype: Qnum 
  
   .. py:method:: Qnum.assignF( prtF, [ U1=0,  prt=PRT_EVEN] )

      Assign a fermionic quantum number to a Qnum object

      :param parityFType prtF: fermionic parity quantum number
      :param int U1: U1 quantum number
      :param parityType prt: parity quantum number
      :return: a Qnum object
      :rtype: Qnum 

   .. py:method:: Qnum.prt()

      Returns the parity quantum number
     
      :return: PRT_EVEN or PRT_ODD
      :rtype: parityType 

   .. py:method:: Qnum.prtF()
   
      Returns the fermionic parity quantun number

      :return: PRTF_EVEN or PRTF_ODD
      :rtype: parityFType 

   .. py:staticmethod:: Qnum.isFermionic()

      Tests whether fermionic parity PRTF_ODD is ever defined.

      :return: True or False
      :rtype: bool

   
   .. rubric:: Attributes

   
   .. py:attribute:: Qnum.U1_LOB = -1000
      
      Minimum allowed U1 quantum number 

   .. py:attribute:: Qnum.U1_UPB = 1000
   
      Maximum allowed U1 quantum number 

.. rubric:: Related data types

.. py:data:: parityType 

   Type of parity quantum number 

   {PRT_EVEN, PRT_ODD}

.. py:data:: parityFType

   Type of fermionic parity quantum number 

   {PRTF_EVEN, PRTF_ODD}
