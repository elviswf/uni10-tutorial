==================
Setting Up pyUni10
================== 

Preparing Your System
---------------------
 You need to have Python 2 installed on your computer.
 
  * If you are using Linux or MacOS X, Python 2 comes with the system. 
  * For the lectures, you also need install iPython.  
  * As an integrated distribution, we recommend the `Anaconda Scientific Python Distribution <https://store.continuum.io/cshop/anaconda/>`_. 

Library Dependencies  
--------------------

pyUni10 requires BLAS and LAPACK libraries to run.  

  * For Linux users, you need to install BLAS and LAPACK.

  * For Mac OS X users, BLAS and LAPACK come with the OS, so you don't have to do anything. 

  * For Windows users, required libraries are supplied in the pyUni10 package.

Obtaining pyUni10
-----------------
Please download  pyUni10 for your architecture:

   * Windows : :download:`pyUni10-0.9.0.Win64.zip <../../pyUni10/pyUni10-0.9.0.Win64.zip>`

   * Mac OS X: :download:`pyUni10-0.9.0.MacOSX.tar.gz <../../pyUni10/pyUni10-0.9.0.MacOSX.tar.gz>`

   * Linux   : :download:`pyUni10-0.9.0.Linux.tar.gz <../../pyUni10/pyUni10-0.9.0.Linux.tar.gz>`

Open a shell terminal, move to the pyUni10 directory, and issue the command:::

    python setup.py install

On Windows, open a command prompt window and issue the command:::

    python.exe setup.py install

.. note:: Depending on the location of your Python installation, you might need the administrator privilege to install.

Runnning Tests
-----------------
Download the test package :download:`pyUni10test.tgz <../../pyUni10/pyUni10test.tgz>` or :download:`pyUni10test.zip <../../pyUni10/pyUni10test.zip>` 

In the ``test`` folder, you should see the following files

* egB1.py
* egM1.py
* egM2.py
* egQ1.py
* egU1.py
* egU2.py
* egU3.py
* egN1.py
* egN1_network
* TestUni10.ipynb

Manually run the Python codes in the sequence listed above, or use the :download:``../../python/test/TestUni10.ipynb``. 

If you don't see any error messages, **congratulations**, you are ready to go!!!
