pyUni10.UniTensor
=================

.. py:currentmodule:: pyUni10

.. py:class:: UniTensor

   Proxy of C++ uni10::UniTensor class.

   Class for symmetric tensors.

.. py:method:: UniTensor([val])

   Construct a rank-0 tensor (scalar)
   
   :param float var: initial value
   :return: a rank-0 tensor (scalar)
   :rtype: UniTensor

.. py:method:: UniTensor(bds, labels, name="")
    
   Construct a UniTensor
   
   :param bds: list of bonds 
   :type bds: array of Bond
   :param labels: list of labels
   :type labels: array of int
   :param str name: name of the tensor
   :return: a UniTensor object
   :rtype: UniTensor

   .. rubric:: Methods

   .. py:method:: UniTensor.assign(bds)
 
      Restructures the *UniTensor*  with bonds bds, and clear all the content.

      :param bds: list of bonds
      :type bds: array of Bonds
      :return: a UniTensor with bonds bds
      :rtype: UniTensor

   .. py:method:: UniTensor.blockNum()
  
      Gives the number of blocks in *UniTensor*
 
      :return: total number of blocks
      :rtype: int

   .. py:method:: UniTensor.blockQnum([idx])

      Returns the quantum number of the idx-th block. 

      If no input is given, returns full list of quantum numbers associated with blocks in *UniTensor*.


      :param int idx: block index
      :return: quantum number(s) 
      :rtype: (array of) Qnum

   .. py:method:: UniTensor.bond([idx])

      Returns the idx-th bond in *UniTensor*.

      If no input is given, returns an array of bonds associated with *UniTensor*.
      
      :return:  bond(s)
      :rtype: (array of) Bond

   .. py:method:: UniTensor.bondNum()
  
      Returns the number of bonds in *UniTensor*.

      :return: number of bonds
      :rtype: int

   .. py:method:: UniTensor.combineBond(labels)

      Combines bonds with *labels*. The resulting bond has the same label and
      bondType as the bond with the first label in *labels*.

      :param list labels: array of labels
      :return: combined bond with the same label and bondType as first bond in *labels*
      :rtype: Bond

   .. py:method:: UniTensor.elemCmp(Tb)
     
      Tests whether the elements of the UniTensor are the same as in Tb.
      

      :param UniTensor Tb: 
      :return: True if the elements of *UniTensor* is the same as in Tb, False otherwise.
      :rtyoe: bool


   .. py:method:: UniTensor.elemNum()


      Returns the number of elements in *UniTensor*.

      :return: number of elements
      :rtype: int

   .. py:method:: UniTensor.getBlock([qnum,] diag=false)
    
      Returns the block elements of quantum number qnum as a Matrix. If the diag flag is set, only the diagonal elements of the block will be picked out to a diagonal Matrix. If qnum is not given, returns the Qnum(0) block. 

      :param Qnum qnum: blcok quantum number
      :param bool diag: If True, output a diagnoal part only
      :return: a Matrix of block of qnum
      :rtype: Matrix

   .. py:method:: UniTensor.getBlocks()

      Returns the a dictionary {qnum:block} of the mapping from Qnum to Matrix .

      :return: mapping from Qnum to Matrix
      :rtype: dict

   .. py:method:: UniTensor.getElem()

      Returns the reference to th elements of *UniTensor*

      :return: Reference to the elements
      :rtype: float *
 
   .. py:method:: UniTensor.getName()

      Returns the name of *UniTensor*
     
      :return: Name of *UniTensor*
      :rtype: str    

   .. py:method:: UniTensor.getRawElem()

      Returns the raw elements of *UniTensor* with row(column) basis defined by 
      the incoming (outgoing) bonds.
 
      :return: raw elements of *UniTensor*
      :rtype: Matrix
 

   .. py:method:: UniTensor.identity([qnum])

      Sets all the block elements to identity. 
      If qnum is given, sets the block with quantum number qnum to identity.

      :param Qnum qnum: quantum number

   .. py:method:: UniTensor.inBondNum()
 
      Returns the number of incoming bonds in *UniTensor*

      :return: number of incoming bonds
      :rtype: int

   .. py:method:: UniTensor.label([idx])

      Returns the label of the idx-th bond. If no input is given, 
      returns an array of labels.

      :param int idx: bond index
      :return: (array of) label(s) 
      :rtype: int

   .. py:method:: UniTensor.orthoRand()

      Returns a Matrix of orthogonal basis vectors with random elements.

      :return: a Matrix of orthogonal basis vectors
      :rtype: Matrix


   .. py:method:: UniTensor.partialTrace(la, lb)

      Traces out bonds of label la and lb, and returns a reference to resulting
      tensor.

      :return: reference to the partial trace of *UniTensor*
      :rtype: float *

   .. py:method:: UniTensor.permute([new_label], inBondNum)

      Permutes the order the bonds according to new_label, and changes 
      the number of  incoming bonds to inBondNum. 

      :return: reference to the permuted *UniTensor*
      :rtype: float *

   .. py:method:: UniTensor.printRawElem()

      Prints the raw elements  of *UniTensor*

   .. py:staticmethod:: UniTensor.profile()

      Prints the memory usage of all the existing *UniTensors* .


   .. py:method:: UniTensor.putBlock([qnum, ] mat)

      Assigns the elements of  Matrix mat into the block with Qnum qnum of 
      *UniTensor*. If qnum is not give, assigns to Qnum(0) block. 

      :param Qnum qnum: quantum number of the block being assigned to 
      :param Matrix mat: matrix to be assigned. 


   .. py:method:: UniTensor.randomize()

      Assigns random numbers between 0 and 1 to the elements of *UniTensor*.

   .. py:method:: UniTensor.save(filename)
  
      Saves the content of *UniTensor* to the binary file *filename*.

   .. py:method:: UniTensor.setElem(elem)
     
      Assigns the elements to *UniTensor*, replacing the originals.

      :param elem: elements
      :type elem: array of float
      

   .. py:method:: UniTensor.setLabel(new_labels)
   
      Assigns *new_labels* to the bonds of *UniTensor*, replacing the orinals.

      :param new_labels: new labels
      :type new_labels: array of int
      

   .. py:method:: UniTensor.setName(name)

      Assigns *name* to *UniTensor*
 
      :param str name: name to be assigned
      
   .. py:method:: UniTensor.setRawElem(rawElem)
   
      Assigns raw elements (non-block-diagonal) to *UniTensor*. 

      :param rawElem: input elements
      :type rawElem: array of float
           
   .. py:method:: UniTensor.set_zero()

      Sets the elements of *UniTensor* to zero. 

   .. py:method:: UniTensor.similar(Tb)

      Tests whether the *UniTensor* is similar to input tensor Tb.
      Two tensors are said to be similar if the bonds of the tensors 
      are exactly the same.
      
      :param UniTensor Tb: tensor to be compared to. 
      :return: True if *UniTensor* and Tb are similar.
      :rtype: bool

   .. py:method:: UniTensor.trace()

      Traces out incoming and outgoing bonds, and returns the trace value.

      :return: trace of *UniTensor*.
      :rtype: float


   .. py:method:: UniTensor.transpose()

      Transpose all blocks associated with quantum numbers. 
      The bonds are changed from incoming to outcoming or vice versa while the 
      quantum numbers remain the same on the bonds. 

      :return: reference to the transposed tensor.
      :rtype: UniTensor &

   
   

   
   
   
