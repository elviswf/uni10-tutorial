pyUni10.PRTF_EVEN
=================

.. py:data:: pyUni10.PRTF_EVEN
   :annotation: = 0

   Value defines particle parity **even** in a fermionic system


