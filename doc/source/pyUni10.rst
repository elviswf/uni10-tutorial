.. _pyUni10:

.. default-domain:: python

pyUni10
=======

.. Documentation string which may be at the top of the module.

.. py:module:: pyUni10

.. Links to members.

pyUni10 Constants
-----------------

The constants defined in pyUni10 are:

.. toctree::
   :maxdepth: 2

   pyUni10.BD_IN      
   pyUni10.BD_OUT
   pyUni10.PRTF_EVEN
   pyUni10.PRTF_ODD
   pyUni10.PRT_EVEN
   pyUni10.PRT_ODD

Global Functions
----------------
.. toctree::
   :maxdepth: 2

   pyUni10.combine
   pyUni10.contract
   pyUni10.otimes
   pyUni10.takeExp

Classes
-------
.. toctree::
   :maxdepth: 2

   pyUni10.Bond
   pyUni10.Matrix
   pyUni10.Network
   pyUni10.Qnum
   pyUni10.UniTensor


