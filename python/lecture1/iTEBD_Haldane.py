import pyUni10 as uni10
import sys
import numpy as np
import copy
# Define Spin-1 operators
def matSp():
  spin = 1
  dim = int(spin * 2 + 1)
  return uni10.Matrix(dim, dim, [0, np.sqrt(2), 0,  0, 0, np.sqrt(2), 0, 0, 0 ])

def matSm():
  spin =1
  dim = int(spin * 2 + 1)
  return uni10.Matrix(dim, dim, [0, 0, 0,  np.sqrt(2), 0, 0,  0, np.sqrt(2), 0 ])

def matSz():
  spin = 1
  dim = int(spin * 2 + 1)
  return uni10.Matrix(dim, dim, [1, 0, 0, 0, 0, 0, 0, 0, -1])

# Large-D state
def LargeD(chi,states):

    Psi=[]
    for i in xrange(states):
        for j in xrange(chi*chi):
            if i==1 and j % chi==0:
                Psi.append(1.0)
            else:
                Psi.append(0.0)

    M=uni10.Matrix(chi,states*chi,Psi)
    return M,M

def Neel(chi,states):

    Psi=[]

    for i in xrange(states):
        for j in xrange(chi*chi):
            if i==0 and j % chi==0:
                Psi.append(1.0)
            else:
                Psi.append(0.0)

    MA=(uni10.Matrix(chi,states*chi,Psi))
    Psi=[]

    for i in xrange(states):
        for j in xrange(chi*chi):
            if i==2 and j % chi==0:
                Psi.append(1.0)
            else:
                Psi.append(0.0)
    MB=(uni10.Matrix(chi,states*chi,Psi))
    return MA,MB
# Define Hamiltonian
def Haldane_Chain(D):
    spin = 1
    sp = matSp()
    sm = matSm()
    sz = matSz()
    sz2= sz*sz
    iden = uni10.Matrix(3,3, [1, 0, 0, 0, 1, 0, 0, 0, 1])
    ham =uni10.otimes(sz,sz)+ 0.5*uni10.otimes(sp,sm)+ 0.5*uni10.otimes(sm,sp)\
        +0.5*float(D)*(uni10.otimes(iden,sz2)+uni10.otimes(sz2,iden))
    dim = int(spin * 2 + 1)
    bdi = uni10.Bond(uni10.BD_IN, dim);
    bdo = uni10.Bond(uni10.BD_OUT, dim);
    H =  uni10.UniTensor([bdi, bdi, bdo, bdo], "Haldane_Chain")
    H.putBlock(ham)
    return H



def bondcat(T, L, bidx):
	labels = T.label();
	per_labels = list(T.label())
	per_labels.insert(0, per_labels.pop(bidx))
	inBondNum = T.inBondNum();
	T.permute(per_labels, 1)
	T.putBlock(L * T.getBlock())
	T.permute(labels, inBondNum);

def bondrm(T, L, bidx):
	invL = uni10.Matrix(L.row(), L.col(), True)
	for i in xrange(L.elemNum()):
		invL[i] = 0 if L[i] == 0 else 1.0 / L[i]
	bondcat(T, invL, bidx)

def find_gs(G,L, N, H, delta):
    U = uni10.UniTensor(H.bond(), "U")
    U.putBlock(uni10.takeExp(-delta, H.getBlock()))
    for step in range(N):
        # Construct theta
        A = step % 2
        B = (step + 1) % 2
        bondcat(G[A], L[A], 1);
        bondcat(G[A], L[B], 0);
        bondcat(G[B], L[B], 1);
        G[A].setLabel([-1, 3, 1]);
        G[B].setLabel([3, -3, 2]);
        U.setLabel([1, 2, -2, -4]);
        theta = uni10.contract(G[A], G[B], True)
        # G[A], G[B] is permuted after the execution
        Ntheta = theta
        theta *= U
        theta.permute([-1, -2, -3, -4], 2);

        # SVD
        svd = theta.getBlock().svd()

        # Truncation
        sv = svd[1]
        norm = sv.resize(chi, chi).norm()
        sv *= 1.0 / norm;
        L[A] = sv
        G[A].putBlock(svd[0].resize(svd[0].row(), chi));
        G[B].putBlock(svd[2].resize(chi, svd[2].col()));
        G[A].permute([-1, 3, 1], 1);
        bondrm(Gs[A], Ls[B], 0);
        bondrm(Gs[B], Ls[B], 1);

        if step % 50 == 0:
            sys.stdout.write('.')
            sys.stdout.flush()
    print ''
    norm=(Ntheta*Ntheta)[0]
    val = (theta * theta)[0]
    # Fix the tensor lables
    #G[0].setLabel([-1, 3, 1])
    #G[1].setLabel([3, -3, 2])
    return -np.log(val) / delta / 2, norm

def measure(G, L, H, Op):
    #labels=G[0].label()

    bondcat(G[0], L[0], 1)
    bondcat(G[0], L[1], 0)
    bondcat(G[1], L[1], 1)

    G[0].setLabel([-1, 3, 1])
    G[1].setLabel([3, -3, 2])
    theta = uni10.contract(G[0], G[1], False)


    theta.setLabel([-1,-2,-3,-4])

    O=uni10.UniTensor(H.bond())
    O.putBlock(Op)

    O.setLabel([-2,-4,-5,-6])

    O_theta=theta*O
    O_theta.permute([-1,-5,-3,-6],2)
    O_theta.setLabel([-1,-2,-3,-4])

    #G[0].permute(labels,1)
    return (theta*O_theta)[0]

chi = 30
delta = 0.1
N = 10000
D = -100
H = Haldane_Chain(D)

bdi_chi = uni10.Bond(uni10.BD_IN, chi);
bdo_chi = uni10.Bond(uni10.BD_OUT, chi);
Gs = []
Gs.append(uni10.UniTensor([bdi_chi, bdo_chi, H.bond(2)], "Ga"))
Gs.append(uni10.UniTensor([bdi_chi, bdo_chi, H.bond(2)], "Gb"))
# Random
#Gs[0].randomize(), Gs[1].randomize()
# Identity
Gs[0].identity(), Gs[1].identity()
# Large-D
# GA,GB= LargeD(chi,H.bond(2).dim())
#Gs[0].putBlock(GA), Gs[1].putBlock(GB)

### Neel
#GA,GB=Neel(chi,H.bond(2).dim())
#Gs[0].putBlock(GA),Gs[1].putBlock(GB),
Ls = []
Ls.append(uni10.Matrix(chi, chi, True))  # Diagonal matrix
Ls.append(uni10.Matrix(chi, chi, True))  # Diagonal matrix
Ls[0].randomize(), Ls[1].randomize()




E, norm=find_gs(Gs,Ls,N,H,delta)
print "norm=",norm
print "E =", E/norm
iden = uni10.Matrix(3,3, [1, 0, 0, 0, 1, 0, 0, 0, 1])
Mz= matSz()
Op_M1=uni10.otimes(Mz,iden)
Op_M2 =uni10.otimes(iden,Mz)

# Staggered Magnetization
Op_Ms = Op_M1+Op_M2*(-1.0)
# Uniform Magnetization
Op_Mu = (Op_M1+Op_M2)

M1=measure(Gs,Ls,H,Op_M1)
print "M1=", (M1/norm)

M2=measure(Gs,Ls,H,Op_M2)




Ms=M1+(-1)*M2
Mu=M1+M2
S_pi=measure(Gs,Ls,H,Op_Ms*Op_Ms)
S_0=measure(Gs,Ls,H,Op_Mu*Op_Mu)
print "M2=", (M2/norm)
print "Ms=", abs(Ms/norm)/2
print "M =", abs(Mu/norm)/2
print "S(pi) =", (S_pi/norm)/2
print "S(0) =", (S_0/norm)/2
